/*
Copyright (c) 2016 Robert Atkinson

All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted (subject to the limitations in the disclaimer below) provided that
the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer.

Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

Neither the name of Robert Atkinson nor the names of his contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESSFOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorController;
import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * This file contains an minimal example of a Linear "OpMode". An OpMode is a 'program' that runs in either
 * the autonomous or the teleop period of an FTC match. The names of OpModes appear on the menu
 * of the FTC Driver Station. When an selection is made from the menu, the corresponding OpMode
 * class is instantiated on the Robot Controller and executed.
 *
 * This particular OpMode just executes a basic Tank Drive Teleop for a PushBot
 * It includes all the skeletal structure that all linear OpModes contain.
 *
 * Use Android Studios to Copy this Class, and Paste it into your team's code folder with a new name.
 * Remove or comment out the @Disabled line to add this opmode to the Driver Station OpMode list
 */

@TeleOp(name="Motor Test", group="Test")  // @Autonomous(...) is the other common choice
public class TestingOpMode extends LinearOpMode {
    public double square(double number) {
        return number*Math.abs(number);
    }

    /* Declaing varables
     * created runtime, not really necessary, but a nice feature.
     */
    private ElapsedTime runtime = new ElapsedTime();

    /* Declared all motors */
    DcMotor frontLeftMotor = null;
    DcMotor frontRightMotor = null;
    DcMotor backLeftMotor = null;
    DcMotor backRightMotor = null;
    DcMotor shooterFront = null;
    DcMotor shooterBack = null;
    DcMotor puLeft = null;
    DcMotor puRight = null;

    // Setup Controllers
    DcMotorController leftDrive = null;
    DcMotorController rightDrive = null;
    DcMotorController pu = null;
    DcMotorController shooter = null;

    @Override
    public void runOpMode() throws InterruptedException {
        // START INITIALIZATION

        telemetry.addData("Status", "Initialized");
        telemetry.update();

        // Initialize Drive Motors

        // Initialized 4 drive motors, each to the name F/B for Front/Back and R/L for Right/Left
        frontLeftMotor  = hardwareMap.dcMotor.get("FL");
        frontRightMotor = hardwareMap.dcMotor.get("FR");
        backLeftMotor  = hardwareMap.dcMotor.get("BL");
        backRightMotor = hardwareMap.dcMotor.get("BR");

        // Mapped motor controllers
        DcMotorController leftDrive = hardwareMap.dcMotorController.get("Leeft Drive");
        DcMotorController rightDrive = hardwareMap.dcMotorController.get("Rit Drive");
        DcMotorController shooter = hardwareMap.dcMotorController.get("Shooti Thing");
        DcMotorController pu = hardwareMap.dcMotorController.get("Liftee Whe");


        java.util.List<DcMotorController> allMotors = hardwareMap.getAll(DcMotorController.class);

        // Reversed all the motors on one side because the motors face opposite directions
        frontLeftMotor.setDirection(DcMotor.Direction.REVERSE);
        backLeftMotor.setDirection(DcMotor.Direction.REVERSE);

        // Initialize shooter motors

        // S = shooter, B/F = back/front
        shooterBack = hardwareMap.dcMotor.get("SB");
        shooterFront = hardwareMap.dcMotor.get("SF");

        shooterFront.setDirection(DcMotor.Direction.REVERSE);

        // Initialize picker-upper motors

        // pu = picker-upper, L/R = left/right
        puLeft = hardwareMap.dcMotor.get("PL");
        puRight = hardwareMap.dcMotor.get("PR");

        // Reverse one because they both face inwards
        puLeft.setDirection(DcMotor.Direction.REVERSE);

        // END INITIALIZATION

        // Wait for the game to start (driver presses PLAY)
        waitForStart();
        runtime.reset(); // Set runtime var to 0

        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {
            // telemetry.addData calls
            telemetry.addData("Controls:", "The way to control the motor powers is to hold the corresponding button then to move the left stick up or down to change the power.");
            telemetry.addData("Front Left (A)", frontLeftMotor.getPower());
            telemetry.addData("Front Right (B)", frontRightMotor.getPower());
            telemetry.addData("Back Left (X)", backRightMotor.getPower());
            telemetry.addData("Back Right (Y)", backLeftMotor.getPower());
            telemetry.addData("Picker-Upper Left (d-pad left)", puLeft.getPower());
            telemetry.addData("Picker-Upper Right (d-pad right)", puRight.getPower());
            telemetry.addData("Back Shooter (d-pad down)", shooterBack.getPower());
            telemetry.addData("Front Shooter (d-pad up)", shooterFront.getPower());
            telemetry.addData("Thingies", allMotors);
            telemetry.update();

            frontLeftMotor.getController();
            frontLeftMotor.getPortNumber();
            // Set the drive motor powers based on the right joystick position
            if (gamepad1.a) {
                // Change the frontLeftMotor power to left stick / 20
                frontLeftMotor.setPower(frontLeftMotor.getPower()+gamepad1.left_stick_y/20);
            }
            if (gamepad1.b) {
                // Change the frontRightMotor power to left stick / 20
                frontRightMotor.setPower(frontRightMotor.getPower()+gamepad1.left_stick_y/20);
            }
            if (gamepad1.x) {
                // Change the frontRightMotor power to left stick / 20
                backRightMotor.setPower(backRightMotor.getPower()+gamepad1.left_stick_y/20);
            }
            if (gamepad1.y) {
                // Change the frontRightMotor power to left stick / 20
                backLeftMotor.setPower(backLeftMotor.getPower()+gamepad1.left_stick_y/20);
            }
            if (gamepad1.dpad_left) {
                // Change the picker upper left power to left stick / 20
                puLeft.setPower(puLeft.getPower()+gamepad1.left_stick_y/20);
            }
            if (gamepad1.dpad_right) {
                // Change the picker upper right power to left stick / 20
                puRight.setPower(puRight.getPower()+gamepad1.left_stick_y/20);
            }
            if (gamepad1.dpad_down) {
                // Change the back shooter power to left stick / 20
                shooterBack.setPower(shooterBack.getPower()+gamepad1.left_stick_y/20);
            }
            if (gamepad1.dpad_up) {
                // Change the front shooter power to left stick / 20
                shooterFront.setPower(shooterFront.getPower()+gamepad1.left_stick_y/20);
            }

            idle(); // Always call idle() at the bottom of your while(opModeIsActive()) loop
        }
    }
}

