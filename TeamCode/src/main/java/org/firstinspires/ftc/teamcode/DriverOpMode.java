/*
Copyright (c) 2016 Robert Atkinson

All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted (subject to the limitations in the disclaimer below) provided that
the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer.

Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

Neither the name of Robert Atkinson nor the names of his contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESSFOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotorController;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.hardware.DcMotor;

/**
 * This file contains an minimal example of a Linear "OpMode". An OpMode is a 'program' that runs in either
 * the autonomous or the teleop period of an FTC match. The names of OpModes appear on the menu
 * of the FTC Driver Station. When an selection is made from the menu, the corresponding OpMode
 * class is instantiated on the Robot Controller and executed.
 *
 * This particular OpMode just executes a basic Tank Drive Teleop for a PushBot
 * It includes all the skeletal structure that all linear OpModes contain.
 *
 * Use Android Studios to Copy this Class, and Paste it into your team's code folder with a new name.
 * Remove or comment out the @Disabled line to add this opmode to the Driver Station OpMode list
 */

@TeleOp(name="Driver Controlled Mode v0.0.0", group="Linear Opmode")  // @Autonomous(...) is the other common choice
public class DriverOpMode extends LinearOpMode {
    public double curve(double number) {
        return Math.pow(number, 3.3);
    }

    /* Declaing varables
     * created runtime, not really necessary, but a nice feature.
     */
    private ElapsedTime runtime = new ElapsedTime();

    /* Declared all motors */
    DcMotor frontLeftMotor = null;
    DcMotor frontRightMotor = null;
    DcMotor backLeftMotor = null;
    DcMotor backRightMotor = null;
    DcMotor shooterFront = null;
    DcMotor shooterBack = null;
    DcMotor puLeft = null;
    DcMotor puRight = null;

    // Setup Controllers
    DcMotorController leftDrive = null;
    DcMotorController rightDrive = null;
    DcMotorController pu = null;
    DcMotorController shooter = null;


    // Setup runtime variables
    boolean shooterToggle = false;
    int iterationsSinceShooterToggleUpdate = 0;

    @Override
    public void runOpMode() throws InterruptedException {
        // START INITIALIZATION

        telemetry.addData("Status", "Initialized");
        telemetry.update();

        // Initialize Drive Motors

        // Initialized 4 drive motors, each to the name F/B for Front/Back and R/L for Right/Left
        frontLeftMotor  = hardwareMap.dcMotor.get("FL");
        frontRightMotor = hardwareMap.dcMotor.get("FR");
        backLeftMotor  = hardwareMap.dcMotor.get("BL");
        backRightMotor = hardwareMap.dcMotor.get("BR");

        // Mapped motor controllers
        DcMotorController leftDrive = hardwareMap.dcMotorController.get("Leeft Drive");
        DcMotorController rightDrive = hardwareMap.dcMotorController.get("Rit Drive");
        DcMotorController shooter = hardwareMap.dcMotorController.get("Shooti Thing");
        DcMotorController pu = hardwareMap.dcMotorController.get("Liftee Whe");

        // Reversed all the motors on one side because the motors face opposite directions
        frontLeftMotor.setDirection(DcMotor.Direction.REVERSE);
        backLeftMotor.setDirection(DcMotor.Direction.REVERSE);

        // Initialize shooter motors

        // S = shooter, B/F = back/front
        shooterBack = hardwareMap.dcMotor.get("SB");
        shooterFront = hardwareMap.dcMotor.get("SF");

        shooterFront.setDirection(DcMotor.Direction.REVERSE);

        // Initialize picker-upper motors

        // pu = picker-upper, L/R = left/right
        puLeft = hardwareMap.dcMotor.get("PL");
        puRight = hardwareMap.dcMotor.get("PR");

        // Reverse one because they both face inwards
        puLeft.setDirection(DcMotor.Direction.REVERSE);

        // END INITIALIZATION

        // Wait for the game to start (driver presses PLAY)
        waitForStart();
        runtime.reset(); // Set runtime var to 0

        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {
            telemetry.addData("Instructions", "To drive, you use the left stick. Up is foreward, down is backward, left is left and right is right. The front of the robot is the scoopy part.");
            telemetry.addData("NOTICE", "You suck at driving. Programmers (like Isaiah) are so much cooler than you ¯\\\\_(ツ)_/¯");
            telemetry.addData("Status", "Front Left Motor Power " + frontLeftMotor.getPower());
            telemetry.addData("Status", "Back Left Motor Power " + backLeftMotor.getPower());
            telemetry.addData("Status", "Front Right Motor Power " + frontRightMotor.getPower());
            telemetry.addData("Status", "Back Right Motor Power " + backRightMotor.getPower());
            telemetry.addData("Shooter Status", shooterToggle);
            telemetry.update();

            // Set the drive motor powers based on the right joystick position
            frontLeftMotor.setPower(-curve(gamepad1.right_stick_y) - curve(gamepad1.right_stick_x));
            backLeftMotor.setPower(-curve(gamepad1.right_stick_y) - curve(gamepad1.right_stick_x));
            frontRightMotor.setPower(-curve(gamepad1.right_stick_y) + curve(gamepad1.right_stick_x));
            backRightMotor.setPower(-curve(gamepad1.right_stick_y) + curve(gamepad1.right_stick_x));

            // Deal with shooterToggle settings
            if (gamepad1.a && iterationsSinceShooterToggleUpdate == 10) {
                iterationsSinceShooterToggleUpdate = 0;
                shooterToggle = !shooterToggle;
            }

            if (iterationsSinceShooterToggleUpdate < 10) {
                iterationsSinceShooterToggleUpdate++;
            }

            // Toggle shooter if button pressed and released
            if (shooterToggle || gamepad1.right_bumper) {
                shooterBack.setPower(1);
                shooterFront.setPower(1);
            } else {
                shooterFront.setPower(0);
                shooterBack.setPower(0);
            }

            // Deal with picker upper
            if (gamepad1.dpad_up) {
                puLeft.setPower(1);
                puRight.setPower(1);
            } else if (gamepad1.dpad_down) {
                puLeft.setPower(-1);
                puRight.setPower(-1);
            } else {
                puLeft.setPower(0);
                puRight.setPower(0);
            }

            idle(); // Always call idle() at the bottom of your while(opModeIsActive()) loop
        }
    }
}

